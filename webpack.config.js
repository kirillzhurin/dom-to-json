const path = require('path')
const glob = require('glob')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = {
  entry: {
    'bundle.js': glob.sync('build/static/js/*.js').map(f => path.resolve(__dirname, f)),
  },
  output: {
    filename: './domToJson.min.js'
  },
  plugins: [new UglifyJsPlugin()],
}