import React, { Component } from 'react';
import domToJson from './lib/domToJson'

class App extends Component {
  options = {
		computedStyle: false,
		deep: true,
		domProperties: false,
		stringify: false
	};

  styles = {
    panel: {
      fontFamily: 'Arial',
      fontSize: '14px',
      color: '#3e4957',
      backgroundColor: '#ecf0f1',
      border: '1px solid #e0e0e0',
      borderRadius: '5px',
      padding: '10px 0',
      alignItems: 'center',
      display: 'flex'
    },
    button: {
      color: '#fff',
      backgroundColor: '#8bc34a',
      border: '1px solid #4caf50',
      padding: '6px 12px',
      cursor: 'pointer',
      fontSize: '14px',
      borderRadius: '5px',
      outline: 'none',
      margin: '0 10px'
    },
    wrap: {
      padding: '0 10px',
      borderRight: '1px solid #e0e0e0',
      boxShadow: '1px 0 rgba(255, 255, 255, 0.3)',
      height: '35px',
      lineHeight: '35px'
    },

    text: {
      outline: 'none',
      color: '#3e4957',
      border: '1px solid #d6dbde',
      padding: '6px 12px',
      fontSize: '14px',
      borderRadius: '5px',
      lineHeight: 'normal'
    }

  }

  handleClick = () => {
    console.log(domToJson.toJSON(document.body, this.options));
  }

  handleChangeDeep = ({target: { value }}) => {
    value = Math.abs(+value);
    this.options.deep = value || true;
  }

  handleChangeCheckbox = ({target: { checked, name }}) => {
    this.options[name] = checked;
  }

  render() {
    return (
      <div style={this.styles.panel}>
        <div style={this.styles.wrap}>
          <input style={this.styles.text} onChange={this.handleChangeDeep} placeholder="Deep" type="text" />
        </div>
        <div style={this.styles.wrap}>
          <label>Stringify:</label>
          <input name="stringify" type="checkbox" onChange={this.handleChangeCheckbox} />
        </div>
        <div style={this.styles.wrap}>
          <label>Computed Style:</label>
          <input name="computedStyle" type="checkbox" onChange={this.handleChangeCheckbox} />
        </div>
        <div style={this.styles.wrap}>
          <label>Dom Properties:</label>
          <input name="domProperties" type="checkbox" onChange={this.handleChangeCheckbox} />
        </div>
        <button style={this.styles.button} onClick={this.handleClick}>DOM to JSON</button>
      </div>
    );
  }
}

export default App;
