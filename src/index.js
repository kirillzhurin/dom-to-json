import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
const container = document.createElement('section');
const id = 'domToJson';
container.setAttribute('id', id);
document.body.prepend(container);
ReactDOM.render(<App exclude={id} />, container);
